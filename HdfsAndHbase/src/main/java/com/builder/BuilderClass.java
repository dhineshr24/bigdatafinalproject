package com.builder;

import com.config.setVariables;
import com.service.bulkupload.BulkLoadDriver;
import com.service.csvtohdfs.runCsvTask;
import com.service.wordcount.WordCountDriver;
import org.apache.hadoop.util.ToolRunner;

public class BuilderClass {
    private String tableName = "peopletable";
    private String inPath  ="hdfs://localhost:8020/WordCount/InputFolder";
    private String outPath ="hdfs://localhost:8020/WordCountOutput";
    private String jobName = "wordcount";

    private String bulkLoadInPathFiles = "hdfs://localhost:8020/CSVFolder";
    private String bulkLoadJobName = "HBase Bulk Load Example";
    private String bulkLoadOuthPath = "hdfs://localhost:8020/BulkUploadHbase";

    public void buildTask(String[] args) throws Exception{
        runCsvTask taskObj = new runCsvTask(setVariables.CREATEPEOPLECSVPATH,
                setVariables.CREATEEMPANDBUILDINGCSVPATH, setVariables.RESOURCE1,
                setVariables.RESOURCE2,tableName);
        taskObj.runTask();

        new WordCountDriver(inPath,jobName,outPath).runWordCountDriver();

       ToolRunner.run(new BulkLoadDriver(bulkLoadInPathFiles,bulkLoadJobName,
                bulkLoadOuthPath, tableName), args);
    }
}
