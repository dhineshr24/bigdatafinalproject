package com.service.employeeleastattendance;

import com.config.setVariables;
import com.util.protoobjects.EmployeeOuterClass;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class AttendanceReducer extends Reducer<IntWritable, ImmutableBytesWritable, IntWritable, IntWritable> {
    HashMap<Integer, Integer> attendanceHashMap;
    private String attendanceUri = "hdfs://localhost:8020/ProtoFiles/attendance.seq";

    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
        attendanceHashMap =
                GetAttendanceHashMap.getAttendanceHasMap(attendanceUri, 15);
        System.out.println("attendanceHashMap : " + attendanceHashMap);
    }

    public void reduce(IntWritable key, Iterable<ImmutableBytesWritable> values, Context con) throws IOException, InterruptedException {
        int minValue = Integer.MAX_VALUE;
        Integer buildingCode = Integer.parseInt(key.toString());
        Integer empWithLowestAttendance = 0;
        Integer empId = 0;
        for (ImmutableBytesWritable value : values) {
            EmployeeOuterClass.Employee.Builder employee =
                    EmployeeOuterClass.Employee.newBuilder().mergeFrom(value.get());

            empId = Integer.parseInt(String.valueOf(employee.getEmployeeId()));
            if (attendanceHashMap.containsKey(empId)==true) {
                int noOfAttendance = attendanceHashMap.get(empId);
                if (noOfAttendance < minValue) {
                    minValue = noOfAttendance;
                    empWithLowestAttendance = empId;
                }
            }
        }
        System.out.println("For building code : " + buildingCode + " employee with minimum " +
                "attendance : " + minValue + " is " + empWithLowestAttendance);
        con.write(new IntWritable(buildingCode), new IntWritable(empId));
    }
}