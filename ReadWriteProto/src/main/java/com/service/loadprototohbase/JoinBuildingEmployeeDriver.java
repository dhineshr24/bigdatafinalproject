package com.service.loadprototohbase;

import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.TableMapReduceUtil;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.apache.hadoop.util.Tool;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class JoinBuildingEmployeeDriver extends Configured implements Tool {
    private String jobName;
    private String buildingTableName;
    private String employeeTableName;
    private String populateCafeteriaCodeForEmployeePath;

    public JoinBuildingEmployeeDriver(String jobName, String buildingTableName,
                                      String employeeTableName, String buildingProtoHbasePath) {
        this.jobName = jobName;
        this.buildingTableName = buildingTableName;
        this.employeeTableName = employeeTableName;
        this.populateCafeteriaCodeForEmployeePath = buildingProtoHbasePath;
    }

    @Override
    public int run(String[] arg0) {
        List<Scan> scans = new ArrayList<Scan>();

        Scan scan1 = new Scan();
        scan1.setAttribute("scan.attributes.table.name", Bytes.toBytes(buildingTableName));
        System.out.println(scan1.getAttribute("scan.attributes.table.name"));
        scans.add(scan1);

        Scan scan2 = new Scan();
        scan2.setAttribute("scan.attributes.table.name", Bytes.toBytes(employeeTableName));
        System.out.println(scan2.getAttribute("scan.attributes.table.name"));
        scans.add(scan2);

        HBaseConfiguration conf = new HBaseConfiguration();
        Job job = null;
        try {
            job = new Job(conf, jobName);

            job.setJarByClass(JoinBuildingEmployeeDriver.class);
            job.setNumReduceTasks(0);
            FileOutputFormat.setOutputPath(job, new Path(populateCafeteriaCodeForEmployeePath));
            job.setOutputFormatClass(SequenceFileOutputFormat.class);
            job.setOutputKeyClass(LongWritable.class);
            job.setOutputValueClass(ImmutableBytesWritable.class);
            TableMapReduceUtil.initTableMapperJob(
                    scans,
                    JoinBuildingEmployeeMapper.class,
                    LongWritable.class,
                    ImmutableBytesWritable.class,
                    job);

            job.waitForCompletion(true);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return 0;
    }

}