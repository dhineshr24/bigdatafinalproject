package com.service.readwriteprototohdfs;

import com.util.protoobjects.BuildingOuterClass;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.apache.hadoop.io.IOUtils;
import org.apache.hadoop.io.SequenceFile;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

public class SetBuildingProto {

    private String buildingCsvFilePath;
    private String uri;

    public SetBuildingProto(String buildingCsvFilePath, String uri) {
        this.buildingCsvFilePath = buildingCsvFilePath;
        this.uri = uri;
    }

    public void setBuildingProto() {
        ArrayList<BuildingOuterClass.Building.Builder> buildingList = new ArrayList<BuildingOuterClass.Building.Builder>();
        Reader reader = null;
        try {
            reader = Files.newBufferedReader(Paths.get(buildingCsvFilePath));
            boolean columnRead = false;
            Iterable<CSVRecord> records = CSVFormat.DEFAULT.parse(reader);
            for (CSVRecord record : records) {
                if (columnRead) {
                    BuildingOuterClass.Building.Builder building = BuildingOuterClass.Building.newBuilder();
                    building.setBuildingCode(Integer.parseInt(record.get(0)));
                    building.setTotalFloors(Integer.parseInt(record.get(1)));
                    building.setCompaniesInTheBuilding(Integer.parseInt(record.get(2)));
                    building.setCafeteriaCode(record.get(3));
                    buildingList.add(building);
                }
                columnRead = true;
            }

            try {
                WriteProtoToHdfs.writeProtoObjectsToHdfs(null, buildingList, null, uri,
                        "building");
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (
                IOException e) {
            e.printStackTrace();
        } finally {
            IOUtils.closeStream(reader);
        }

    }
}
