package com.service.readwriteprototohdfs;

import com.util.protoobjects.AttendanceOuterClass;
import com.util.protoobjects.BuildingOuterClass;
import com.util.protoobjects.EmployeeOuterClass;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.io.IOUtils;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.SequenceFile;
import org.apache.hadoop.conf.Configuration;

import java.io.IOException;
import java.util.ArrayList;

public class ReadProtoFromHdfs {
    String uri;
    Configuration config;

    public ReadProtoFromHdfs(String uri, Configuration conf) throws Exception {
        this.uri = uri;
        this.config = conf;
    }

    public void readProtoObjectsFromHdfs(ArrayList<EmployeeOuterClass.Employee.Builder> employeeList,
                                         ArrayList<BuildingOuterClass.Building.Builder> buildingList,
                                         ArrayList<AttendanceOuterClass.Attendance.Builder> attendanceList,
                                         String protoObject) {
        /* Create a SequenceFile Reader object.*/
        SequenceFile.Reader reader = null;
        try {
            FileSystem fs = FileSystem.get(config);
            IntWritable key = new IntWritable();
            ImmutableBytesWritable value = new ImmutableBytesWritable();
            Path path = new Path(uri);
            reader = new SequenceFile.Reader(config, SequenceFile.Reader.file(path));
            System.out.println("Reading " + protoObject);
            if (protoObject.equals("employee")) {
                while (reader.next(key, value)) {
                    System.out.println(key + "\t" +
                            EmployeeOuterClass.Employee.newBuilder().mergeFrom(value.get()));
                }
            } else if (protoObject.equals("building")) {
                while (reader.next(key, value)) {
                    System.out.println(key + "\t" +
                            BuildingOuterClass.Building.newBuilder().mergeFrom(value.get()));
                }
            } else if (protoObject.equals("attendance")) {
                while (reader.next(key, value)) {
                    System.out.println(key + "\t" +
                            AttendanceOuterClass.Attendance.newBuilder().mergeFrom(value.get()));
                }
            }

            reader.close();
        } catch (
                IOException e) {
            e.printStackTrace();
        } finally {
            IOUtils.closeStream(reader);
        }

    }
}
