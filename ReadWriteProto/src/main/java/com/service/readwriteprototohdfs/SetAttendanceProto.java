package com.service.readwriteprototohdfs;

import com.google.protobuf.Timestamp;
import com.util.protoobjects.AttendanceOuterClass;

import java.time.Instant;
import java.time.Period;
import java.util.ArrayList;
import java.util.Random;

public class SetAttendanceProto {
    private final String ATTENDANCE_START_TIMESTAMP;
    private final int ATTENDANCE_NO_OF_DAYS;
    private String uri;

    public SetAttendanceProto(String ATTENDANCE_START_TIMESTAMP, int ATTENDANCE_NO_OF_DAYS, String uri) {
        this.ATTENDANCE_START_TIMESTAMP = ATTENDANCE_START_TIMESTAMP;
        this.ATTENDANCE_NO_OF_DAYS = ATTENDANCE_NO_OF_DAYS;
        this.uri = uri;
    }

    public void setAttendanceProto() {
        Random random_no = new Random();
        ArrayList<AttendanceOuterClass.Attendance.Builder> attendanceList =
                new ArrayList<AttendanceOuterClass.Attendance.Builder>();

        for (int employee_id = 1; employee_id <= 100; employee_id++) {
            AttendanceOuterClass.Attendance.Builder attendance =
                    AttendanceOuterClass.Attendance.newBuilder();
            attendance.setEmployeeId(employee_id);
            Instant instant
                    = Instant.parse(ATTENDANCE_START_TIMESTAMP);
            AttendanceOuterClass.DatePresence.Builder datePresence =
                    AttendanceOuterClass.DatePresence.newBuilder();
            for (int i = 1; i <= ATTENDANCE_NO_OF_DAYS; i++) {
                Timestamp timestamp = Timestamp.newBuilder().setSeconds(instant.getEpochSecond())
                        .setNanos(instant.getNano()).build();
                datePresence.setDate(timestamp);
                boolean is_present = random_no.nextBoolean();
                datePresence.setIsPresent(is_present);
                instant = instant.plus(Period.ofDays((1)));
                attendance.addDatesPresent(datePresence);
            }
            attendanceList.add(attendance);
        }
        try {
            WriteProtoToHdfs.writeProtoObjectsToHdfs(null, null, attendanceList, uri,
                    "attendance");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
